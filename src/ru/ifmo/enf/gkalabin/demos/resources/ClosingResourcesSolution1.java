package ru.ifmo.enf.gkalabin.demos.resources;

import java.io.IOException;

/**
 * @author gkalabin@papeeria.com
 */
public class ClosingResourcesSolution1 {
  /**
   * Вариант решения проблемы номер 1: работает для java 6, но весьма многословный
   */
  public static void main(String[] args) {
    DemoStream os = null;
    try {
      os = new DemoStream("demo.txt");
      os.write("Important data\n");
      os.write("Another important data\n");
      os.write("We have no free space for this\n");
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (os != null) {
        try {
          os.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
