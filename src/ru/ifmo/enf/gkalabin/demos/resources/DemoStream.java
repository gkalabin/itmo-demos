package ru.ifmo.enf.gkalabin.demos.resources;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author gkalabin@papeeria.com
 */
public class DemoStream extends OutputStream {
  private final OutputStream myUnderlyingStream;
  private int myRemainingSpace = 3;

  public DemoStream(String fileName) throws FileNotFoundException {
    myUnderlyingStream = new BufferedOutputStream(new FileOutputStream(fileName));
  }

  @Override
  public void write(int b) throws IOException {
    myUnderlyingStream.write(b);
  }
  
  public void write(String s) throws IOException {
    if (--myRemainingSpace <= 0) {
      throw new IOException("Hey! We have a problem");
    }
    write(s.getBytes());
  }

  @Override
  public void close() throws IOException {
    myUnderlyingStream.close();
  }
}
