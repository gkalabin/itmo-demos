package ru.ifmo.enf.gkalabin.demos.resources;

import java.io.IOException;

/**
 * @author gkalabin@papeeria.com
 */
public class ClosingResourcesProblem1 {
  /**
   * Здесь демонстрируется проблема:
   * файл будет пустой, хотя места не хватило лишь для последней строки.
   */
  public static void main(String[] args) throws IOException {
    DemoStream os = new DemoStream("demo.txt");
    os.write("Important data\n");
    os.write("Another important data\n");
    os.write("We have no free space for this\n");
    os.close();
  }
}
