package ru.ifmo.enf.gkalabin.demos.resources;

import java.io.IOException;

/**
 * @author gkalabin@papeeria.com
 */
public class ClosingResourcesBad1 {
  /**
   * Здесь всё работает, но проблема есть.
   */
  public static void main(String[] args) throws IOException {
    DemoStream os = new DemoStream("demo.txt");
    os.write("Important data\n");
    os.write("Another important data\n");
    os.close();
  }
}
