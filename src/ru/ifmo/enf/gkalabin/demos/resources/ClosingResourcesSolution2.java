package ru.ifmo.enf.gkalabin.demos.resources;

import java.io.IOException;

/**
 * @author gkalabin@papeeria.com
 */
public class ClosingResourcesSolution2 {
  /**
   * Более краткое решение, но требуется java 7.
   */
  public static void main(String[] args) throws IOException {
    try (DemoStream os = new DemoStream("demo.txt")) {
      os.write("Important data\n");
      os.write("Another important data\n");
      os.write("We have no free space for this\n");
    }
  }
}
